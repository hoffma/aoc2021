#include <stdio.h>
#include <stdlib.h>

#define PART1 0

#if 1
#define FILENAME "input1"
#else
#define FILENAME "sample"
#endif

int grid[1024][1024];
int max_x;
int max_y;

void draw_line(int x1, int y1, int x2, int y2) {
    int start_x = x1; // (x1 > x2) ? x2 : x1;
    int start_y = y1; // (y1 > y2) ? y2 : y1;

    int step_x = 0;
    int step_y = 0;

    if(x2 > x1) {
        step_x = 1;
    } else if(x2 < x1) {
        step_x = -1;
    }
    if(y2 > y1) {
        step_y = 1;
    } else if(y2 < y1) {
        step_y = -1;
    }

    
    /*printf("start_x %d, start_y %d, step_x %d, step_y %d\n", start_x, start_y, step_x, step_y);*/
    
    while((x1 != x2) || (y1 != y2)) {
        grid[y1][x1]++;
        x1 += step_x;
        y1 += step_y;
    }
    grid[y1][x1]++;
}

int get_score() {
    int score = 0;
    for(int j = 0; j < max_y+1; j++) {
        for(int i = 0; i < max_x+1; i++) {
            score += (grid[j][i] > 1) ? 1 : 0;
        }
    }
    return score;
}

void init_lists(char *fn) {
    FILE *fp = fopen(fn, "r");
    int x1,y1,x2,y2;
    max_x = 0;
    max_y = 0;

    if (fp == NULL)
        exit(EXIT_FAILURE);

    for(int i = 0; i < 1024; i++) {
        for(int j = 0; j < 1024; j++) {
        grid[j][i] = 0;
        }
    }

    while(fscanf(fp, "%d,%d -> %d,%d", &x1, &y1, &x2, &y2) == 4) {
        /*printf("(%d, %d) -> (%d, %d)\n", x1, y1, x2, y2);*/

        max_x = (x1 > max_x) ? x1 : max_x;
        max_x = (x2 > max_x) ? x2 : max_x;
        max_y = (y1 > max_y) ? y1 : max_y;
        max_y = (y2 > max_y) ? y2 : max_y;

        /*int a, b, c;
        if(x1 == x2) {
            a = x1;
            b = y1;
            c = y2;
        }*/

#if 0
        if(x1 == x2 || y1 == y2) {
            draw_line(x1, y1, x2, y2);
        }
#else
        draw_line(x1, y1, x2, y2);
#endif

    }
    printf("Score: %d\n\n", get_score());
}

void print_grid() {
    for(int j = 0; j < max_y+1; j++) {
        for(int i = 0; i < max_x+1; i++) {
            printf("%d ", grid[j][i]);
        }
        printf("\n");
    }
}


int part1(char *fn) {
    return 0;
}

int part2(char *fn) {

    return 0;
}


int main() {
    init_lists(FILENAME);
    /*print_grid();*/
    printf("part1: %d\n", part1(FILENAME));
    /*printf("part2: %d\n", part2(FILENAME));*/


    return 0;
}
