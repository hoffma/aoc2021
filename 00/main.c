#include <stdio.h>
#include <stdlib.h>


int part1(char *fn) {

    FILE *fp = fopen(fn, "r");

    if (fp == NULL)
        exit(EXIT_FAILURE);

    return 0;
}

int part2(char *fn) {

    FILE *fp = fopen(fn, "r");
    if(fp == NULL) {
        exit(EXIT_FAILURE);
    }

    return 0;
}

int main() {

    printf("part1: %d\n", part1("input1"));
    printf("part1: %d\n", part2("input2"));

    return 0;
}
