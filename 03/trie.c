#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct trie_t {
    int bit0;
    int bit1;
    struct trie_t *left;
    struct trie_t *right;
};


struct trie_t* new_node() {
    struct trie_t *t = (struct trie_t *) malloc(sizeof(struct trie_t));
    t->bit0 = 0;
    t->bit1 = 0;
    t->left = NULL;
    t->right = NULL;

    return t;
}

void append(struct trie_t *t, char *line) {
    int len = strlen(line)-1;

    for(int i = 0; i < len; i++) {
        char c = line[i];
        switch(c) {
            case '0':
                t->bit0++;
                if(t->left == NULL) {
                    t->left = new_node();
                }
                t = t->left;
                break;
            case '1':
                t->bit1++;
                if(t->right == NULL) {
                    t->right = new_node();
                }
                t = t->right;
                break;
            default:
                fprintf(stderr, "ERROR ON APPEND! Char '%c' found\n", c);
                exit(EXIT_FAILURE);
        }
    }
}

void print_trie(struct trie_t *t, int depth) {
    if(t->left != NULL) 
        print_trie(t->left, ++depth);
    else if(t->right != NULL)
        print_trie(t->right, ++depth);

    printf("%d, 0: %d, 1: %d\n", depth, t->bit0, t->bit1);
}

/* SO MUCH COPY PASTE WOW. AMAZIN' */
int find_oxygen(struct trie_t *t, int oxygen) {
    if(t->bit1 >= t->bit0) {
        if(t->right != NULL) {
            printf("1");
            return find_oxygen(t->right, (oxygen << 1) | 1);
        } else if(t->left != NULL) {
            printf("0");
            return find_oxygen(t->left, (oxygen << 1));
        } else {
            printf("\n");
            return oxygen;
        }
    } else {
        if(t->left != NULL) {
            printf("0");
            return find_oxygen(t->left, (oxygen << 1));
        } else if(t->right!= NULL) {
            printf("1");
            return find_oxygen(t->right, (oxygen << 1));
        } else {
            printf("\n");
            return oxygen;
        }
    }
}

int find_scrubber(struct trie_t *t, int scrubber) {
    if(t->bit1 < t->bit0) {
        if(t->right != NULL) {
            printf("1");
            return find_scrubber(t->right, (scrubber << 1) | 1);
        } else if(t->left != NULL) {
            printf("0");
            return find_scrubber(t->left, (scrubber << 1));
        } else {
            printf("\n");
            return scrubber;
        }
    } else {
        if(t->left != NULL) {
            printf("0");
            return find_scrubber(t->left, (scrubber << 1));
        } else if(t->right!= NULL) {
            printf("1");
            return find_scrubber(t->right, (scrubber << 1) | 1);
        } else {
            printf("\n");
            return scrubber;
        }
    }
}

int main() {

    struct trie_t *init;
    init = new_node();

    FILE *fp = fopen("input2", "r");
    char *line = NULL;
    size_t len;

    if (fp == NULL)
        exit(EXIT_FAILURE);

    while(getline(&line, &len, fp) != -1) {
        /*printf("%s", line);*/
        append(init, line);
    }

    
    printf("\n%d\n", find_oxygen(init, 0)*find_scrubber(init, 0));


}
