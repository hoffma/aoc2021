#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


size_t elements[1024] = {0};
size_t elem_cnt = 0;

size_t binstr2int(char *s) {
    size_t erg = 0;
    size_t bits = strlen(s)-1;

    while(bits > 0) {
        erg += (s[0] - '0') * (size_t) pow(2, (bits - 1));
        s++;
        bits--;
    }

    return erg;
}

void printbin(size_t bin, size_t bitCnt) {
    for(int i = bitCnt-1; i >= 0; i--) {
        printf("%lu", (bin >> i)&0x1);
    }
    printf("\n");
}

int part1(char *fn, size_t bitlen) {

    FILE *fp = fopen(fn, "r");
    char *line = NULL;
    size_t len;

    if (fp == NULL)
        exit(EXIT_FAILURE);

    int bitcnt1[bitlen];
    int bitcnt0[bitlen];
    size_t gamma = 0;
    size_t epsilon = 0;
    size_t mask = 0;
    for(int i = 0; i < bitlen; i++) bitcnt1[i] = 0;
    for(int i = 0; i < bitlen; i++) bitcnt0[i] = 0;

    while(getline(&line, &len, fp) != -1) {
        for(int i = 0; i < bitlen; i++) {
            if(line[i] == '1') bitcnt1[i]++;
            else bitcnt0[i]++;
        }
    }
    for(int i = 0; i < bitlen; i++) {
        gamma <<= 1;
        mask |= (1 << i);
        if(bitcnt1[i] > bitcnt0[i])
            gamma |= 1;
    }
    epsilon = ~gamma&mask;

    return epsilon*gamma;
}

enum {
    SCRUBBER,
    OXYGEN
};

void part2_filter(int bit_pos, int oxygen) {
    int mask = (1 << bit_pos);
    size_t new_elements[elem_cnt];
    size_t new_cnt = 0;
    int sum = 0;

    for(int i = 0; i < elem_cnt; i++) {
        sum += (elements[i] & mask) ? 1 : 0;
    }

    int most_common = (oxygen) ? (sum >= (elem_cnt-sum)) : !(sum >= (elem_cnt - sum));

    for(int i = 0; i < elem_cnt; i++) {
        if(((elements[i] >> bit_pos)&0x1) == most_common) {
            new_elements[new_cnt++] = elements[i];
        }
    }

    for(int i = 0; i < new_cnt; i++) {
        elements[i] = new_elements[i];
    }
    elem_cnt = new_cnt;
}

void part2_print_elem() {
    for(int i = 0; i < elem_cnt; i++) {
        printbin(elements[i], 12);
    }
}

void part2_init(char *fn) {
    FILE *fp = fopen(fn, "r");
    char *line = NULL;
    size_t len = 0;

    if (fp == NULL)
        exit(EXIT_FAILURE);

    while(getline(&line, &len, fp) != - 1) {
        elements[elem_cnt++] = binstr2int(line);
    }

    fclose(fp);
}

int part2_new(char *fn, size_t bitlen) {

    int oxygen, scrubber;

    part2_init(fn);
    int bitpos = bitlen-1;
    while(elem_cnt > 1 && bitpos >= 0) {
        part2_filter(bitpos--, OXYGEN);
    }
    part2_print_elem();
    oxygen = elements[0];

    part2_init(fn);
    bitpos = bitlen-1;
    while(elem_cnt > 1 && bitpos >= 0) {
        part2_filter(bitpos--, SCRUBBER);
    }
    part2_print_elem();
    scrubber = elements[0];

    return oxygen*scrubber;
}

int main() {

    /*printf("part1: %d\n", part1("input1", 12));*/
    printf("part1: %d\n", part2_new("input2", 12));

    return 0;
}
