#include <stdio.h>
#include <stdlib.h>

#define GRID_WIDTH 5
#define GRID_HEIGHT 5
#define GRID_CELLS (GRID_WIDTH*GRID_HEIGHT)
#define GRID_STRIDE 5

#if 0
#define FILENAME "sample"
#else
#define FILENAME "input1"
#endif

struct grid_list {
    int cells[GRID_CELLS];
    int marked[GRID_CELLS];
    int done;
    struct grid_list *next;
    struct grid_list *prev;
};

int *num_list;
int numcnt;
struct grid_list *gl_start;


struct grid_list *new_grid() {
    struct grid_list *n = malloc(sizeof(struct grid_list));
    n->done = 0;
    n->next = NULL;
    n->prev = NULL;

    return n;
}

void print_grid(struct grid_list *g) {
    for(int i = 0; i < GRID_HEIGHT; i++) {
        for(int j = 0; j < GRID_WIDTH; j++) {
            if(g->marked[(i*GRID_HEIGHT) + j] == 1) {
                printf("\033[0;32m");
            }
            printf("%2d ", g->cells[(i*GRID_HEIGHT) + j]);
            printf("\033[0;37m");
        }
        printf("\n");
    }
}

void print_all(struct grid_list *start) {
    int n = 0;
    while(start != NULL) {
        if(!start->done) {
            printf("Grid %d\n", n++);
            printf("---------------------\n");
            print_grid(start);
            printf("#####################\n");
        }
        start = start->next;
    }
    printf("END\n");
}

int count_nodes(struct grid_list *start) {
    struct grid_list *curr = start;
    int cnt = 0;

    while(curr != NULL) {
        cnt += (!curr->done) ? 1 : 0;
        curr = curr->next;
    }

    return cnt;
}

int remove_node(struct grid_list *n) {
    if(count_nodes(gl_start) == 1) { /* cannot remove last node */
        fprintf(stderr, "ONLY ONE NODE IN LIST. CAN'T REMOVE\n");
        return -1;
    } else if(n->prev == NULL) { /* remove first node */
        gl_start = n->next;
        free(n);
        n = NULL;
    } else if(n->next == NULL) { /* remove last node */
        n->prev->next = NULL;
        free(n);
    } else {
        n->prev->next = n->next;
        n->next->prev = n->prev;
        free(n);
    }

    return 0;
}

int check_bingo(int num, struct grid_list *g) {

    for(int i = 0; i < GRID_CELLS; i++) {
        /* number found. check bingo */
        if(g->cells[i] == num) {
            g->marked[i] = 1;
            /*g->cells[i] = -1;*/

            int off_y = i%GRID_HEIGHT;
            int off_x = i/GRID_WIDTH;

            int bingo_v = 1;
            int bingo_h = 1;

            for(int j = 0; j < GRID_WIDTH; j++) {
                if(g->marked[off_x*GRID_HEIGHT + j] != 1) {
                    bingo_v = 0;
                }
                if(g->marked[j*GRID_HEIGHT + off_y] != 1) {
                    bingo_h = 0;
                }
            }
            g->done = (bingo_h | bingo_v);

            return (bingo_h | bingo_v) ? num : 0;
        }
    }


    return 0;
}

int get_unmarked_sum(struct grid_list *g)
{
    int sum = 0;
    for(int i = 0; i < GRID_CELLS; i++) {
        sum += (g->marked[i] ? 0 : g->cells[i]);
    }

    return sum;
}

void init_lists(char *fn) {
    FILE *fp = fopen(fn, "r");
    int num; 
    numcnt = 0;
    char c;

    if (fp == NULL)
        exit(EXIT_FAILURE);

    /* first line is just the numbers. */
    num_list = malloc(1024*sizeof(int));
    while(fscanf(fp, "%d%c", &num, &c) != -1) {
        /*printf("%d, %c\n", num, c);*/
        num_list[numcnt++] = num;

        if(c == '\n') break;
    }

    /* read in the grids */
    int n = 0;
    gl_start = new_grid();
    struct grid_list *curr = gl_start;
    struct grid_list *prev = NULL;
    while(fscanf(fp, "%d%c", &num, &c) != -1) {
        curr->cells[n] = num;
        curr->marked[n] = 0;
        n++;
        if(n >= GRID_CELLS) {
            n = 0;
            curr->next = new_grid();
            curr->prev = prev;
            prev = curr;
            curr = curr->next;
            curr->prev = prev;
        }
    }
    remove_node(curr);
}

int part1(char *fn) {
    struct grid_list *curr = gl_start;
    printf("##### START PART 1 #####\n");
    /*print_all(gl_start);*/

    for(int i = 0; i < numcnt; i++) {
        curr = gl_start;
        /*printf("NUM: %d\n", num_list[i]);*/
        while(curr != NULL) {
            /*printf("Grid %d, nodes: %d\n", n++, count_nodes(gl_start));
            printf("---------------------\n");*/
            int bingo = check_bingo(num_list[i], curr);
            /*printf("bingo: %d\n", bingo);
            print_grid(curr);*/
            if(bingo) {
                int sum = get_unmarked_sum(curr);
                printf("BINGO BINGO BINGO! %d\n", num_list[i]);
                printf("sum: %d\n", sum);
                printf("score: %d\n", sum*num_list[i]);
                return (sum*num_list[i]);
            }
            /*printf("=====================\n");*/

            curr = curr->next;
        }
        /*printf("\n\n");*/
    }

    return 0;
}

int part2(char *fn) {
    struct grid_list *curr = gl_start;
    struct grid_list *prev = NULL;
    printf("##### START PART 2 #####\n");
    /*print_all(gl_start);*/

    int n = 0;
    int finished = 0;
    int found_num = 0;
    int sum = 0;
    for(int i = 0; i < numcnt; i++) {
        n = 0;
        curr = gl_start;
        printf("NUM: %d\n", num_list[i]);
        while(curr != NULL) {
            if(!curr->done) {
                printf("Grid %d, nodes: %d, done: %d\n", n++, count_nodes(gl_start), curr->done);
                printf("---------------------\n");
                print_grid(curr);
                printf("=====================\n");
                check_bingo(num_list[i], curr);
            }
            /*int bingo = check_bingo(num_list[i], curr);*/
            /*printf("bingo: %d\n", bingo);*/
            /*if(bingo) {
                struct grid_list *tmp = curr->next;
                if(remove_node(curr) == -1) {
                    int sum = get_unmarked_sum(curr);
                    printf("BINGO BINGO BINGO! %d\n", num_list[i]);
                    printf("sum: %d\n", sum);
                    printf("score: %d\n", sum*num_list[i]);
                    return (sum*num_list[i]);
                } else {
                    curr = tmp;
                }
            } else {
            }*/
            if(count_nodes(gl_start) == 0) {
                sum = get_unmarked_sum(curr);
                printf("BINGO BINGO BINGO! %d\n", num_list[i]);
                printf("sum: %d\n", sum);
                printf("score: %d\n", sum*num_list[i]);

                printf("Grid %d, nodes: %d, done: %d\n", n++, count_nodes(gl_start), curr->done);
                printf("---------------------\n");
                print_grid(curr);
                printf("=====================\n");
                return (sum*num_list[i]);
            }
            
            prev = curr;
            curr = curr->next;
        }
        if(count_nodes(gl_start) == 0) {
            found_num = num_list[i];
            curr = prev;
            break;
        }
        printf("\n\n");
    }

    return 0;
}


int main() {
    init_lists(FILENAME);
    /*printf("part1: %d\n", part1(FILENAME));*/
    printf("part2: %d\n", part2(FILENAME));


    return 0;
}
