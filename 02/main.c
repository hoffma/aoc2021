#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int part1(char *fn) {

    int depth = 0;
    int pos = 0;

    FILE *fp = fopen(fn, "r");

    if (fp == NULL)
        exit(EXIT_FAILURE);

    char *line = NULL;
    size_t len;

    /*while(fscanf(fp, "%s", s) != EOF) {*/
    while(getline(&line, &len, fp) != -1) {
        char *dir;
        char *tmp_line;
        int val;

        dir = strtok_r(line, " ", &tmp_line);
        val = atoi(strtok(tmp_line, ""));
        /*printf("dir: %s, val: %d\n", dir, val);*/

        if(strcmp(dir, "forward") == 0) {
            pos += val;
        } else if(strcmp(dir, "down") == 0) {
            depth += val;
        } else if(strcmp(dir, "up") == 0) {
            depth -= val;
        }
        /*printf("pos %d, depth %d\n", pos, depth);*/
    }


    return pos*depth;
}

int part2(char *fn) {

    int depth = 0;
    int pos = 0;
    int aim = 0;

    FILE *fp = fopen(fn, "r");
    if(fp == NULL) {
        exit(EXIT_FAILURE);
    }

    char *line = NULL;
    size_t len;

    /*while(fscanf(fp, "%s", s) != EOF) {*/
    while(getline(&line, &len, fp) != -1) {
        char *dir;
        char *tmp_line;
        int val;

        dir = strtok_r(line, " ", &tmp_line);
        val = atoi(strtok(tmp_line, ""));
        /*printf("dir: %s, val: %d\n", dir, val);*/

        if(strcmp(dir, "forward") == 0) {
            pos += val;
            depth += (aim*val);
        } else if(strcmp(dir, "down") == 0) {
            aim += val;
        } else if(strcmp(dir, "up") == 0) {
            aim -= val;
        }
        /*printf("pos %d, depth %d, aim %d\n", pos, depth, aim);*/
    }

    return depth*pos;
}

int main() {

    printf("part1: %d\n", part1("input1"));
    printf("part1: %d\n", part2("input2"));

    return 0;
}
