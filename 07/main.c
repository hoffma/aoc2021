#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <strings.h>
#include <limits.h>

#define PART1 0

#if 1
#define FILENAME "input1"
#else
#define FILENAME "sample"
#endif

#define MAX_ENTRIES 2000

int64_t table[MAX_ENTRIES];
int64_t mx, mn;

int64_t fuel_column(int64_t col) {
    int64_t fuelsum = 0;
    for(int64_t i = mn; i <= mx; i++) {
        if(table[i] > 0) {
            int64_t diff = table[i]*labs(i - col);
            fuelsum += (diff >= 0 ? diff : -diff);  
        }
                /*labs(table[i] - col);*/
    }
    return fuelsum;
}

int64_t fuel_column_rate(int64_t col) {
    int64_t fuelsum = 0;
    for(int64_t i = mn; i <= mx; i++) {
        if(table[i] > 0) {
            int64_t n = labs(i-col);
            int64_t cost = n*(n+1)/2;
            int64_t diff = table[i]*cost;
            fuelsum += (diff >= 0 ? diff : -diff);  
        }
                /*labs(table[i] - col);*/
    }
    return fuelsum;
}

void print_table() {
    for(int64_t i = mn; i <= mx; i++) {
        if(table[i] > 0)
            printf("%ld: %ld\n", i, table[i]);
    }
}

int64_t part1(char *fn) {
    FILE *fp = fopen(fn, "r");
    int64_t pos = 0;
    char c;
    mn = LLONG_MAX;
    mx = LLONG_MIN;

    bzero(table, MAX_ENTRIES*sizeof(int64_t));

    if (fp == NULL)
        exit(EXIT_FAILURE);


    while(fscanf(fp, "%lu%c", &pos, &c) == 2) {
        table[pos]++;
        if(pos < mn) mn = pos;
        if(pos > mx) mx = pos;

        if(c == '\n') {
            break;
        }
    }
    fclose(fp);
    printf("min: %ld, max: %ld\n", mn, mx);
    /*print_table();*/

    int64_t res = LLONG_MAX;
    for(int i = mn; i <= mx; i++) {
        int64_t tmp = fuel_column(i);
        res = (tmp < res) ? tmp : res;
    }

    return res;
}

int64_t part2(char *fn) {
    FILE *fp = fopen(fn, "r");
    int64_t pos = 0;
    char c;
    mn = LLONG_MAX;
    mx = LLONG_MIN;

    bzero(table, MAX_ENTRIES*sizeof(int64_t));

    if (fp == NULL)
        exit(EXIT_FAILURE);


    while(fscanf(fp, "%lu%c", &pos, &c) == 2) {
        table[pos]++;
        if(pos < mn) mn = pos;
        if(pos > mx) mx = pos;

        if(c == '\n') {
            break;
        }
    }
    fclose(fp);
    printf("min: %ld, max: %ld\n", mn, mx);
    /*print_table();*/

    int64_t res = LLONG_MAX;
    for(int i = mn; i <= mx; i++) {
        int64_t tmp = fuel_column_rate(i);
        res = (tmp < res) ? tmp : res;
    }

    return res;
}


int main() {
    /*print_grid();*/
    printf("part1: %ld\n", part1(FILENAME));
    printf("part2: %ld\n", part2(FILENAME));


    return 0;
}
