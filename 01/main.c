#include <stdio.h>
#include <stdlib.h>

struct window {
    int one;
    int two;
    int three;
    int four;
};

void push_window(struct window *w, int val) {
    w->one = w->two;
    w->two = w->three;
    w->three = w->four;
    w->four = val;
}

// #define PUSH(s, x) (s.one = s.two; s.two = s.three; s.three = x;)
#define SUM_FIRST(s) (s.one + s.two + s.three)
#define SUM_SECOND(s) (s.two + s.three + s.four)

int part1(char *fn) {
    int val = 0;
    int prev = -1;
    int cnt = 0;
    FILE *fp = fopen(fn, "r");

    if (fp == NULL)
        exit(EXIT_FAILURE);
    while(fscanf(fp, "%d", &val) != EOF) {
        cnt += (prev != -1 && prev < val) ? 1 : 0;
        prev = val;
    }

    fclose(fp);

    return cnt;
}

int part2(char *fn) {

    struct window win = {0, 0, 0, 0};

    int cnt = 0;
    int tmp;
    FILE *fp = fopen(fn, "r");

    for(int i = 0; i < 3; i++) {
        if(fscanf(fp, "%d", &tmp) < 0) {
            exit(EXIT_FAILURE);
        } else {
            push_window(&win, tmp);
        }
    }
    while(fscanf(fp, "%d", &tmp) != EOF) {
        push_window(&win, tmp);
        if(SUM_SECOND(win) > SUM_FIRST(win)) cnt++;
    }

    fclose(fp);

    return cnt;
}

int main() {
    printf("Part1 erg: %d\n", part1("input1"));
    printf("Part2 erg: %d\n", part2("input2"));

    return 0;
}
