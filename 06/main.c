#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#define PART1 0

#if 1
#define FILENAME "input1"
#else
#define FILENAME "sample"
#endif

#define N 9 /* 0 - 8 */

long table[N], next_table[N];

void show_table() {
    for(long i = 0; i < N; i++) {
        printf("%2lu: %3lu\n", i, table[i]);
    }
}

void next_day() {
    bzero(next_table, N);
    next_table[0] = table[1];
    next_table[1] = table[2];
    next_table[2] = table[3];
    next_table[3] = table[4];
    next_table[4] = table[5];
    next_table[5] = table[6];
    next_table[6] = table[7];
    next_table[7] = table[8];

    next_table[6] += table[0];
    next_table[8] = table[0];

    for(long i = 0; i < N; i++)
        table[i] = next_table[i];
}

long count_fish() {
    long cnt = 0;
    for(long i = 0; i < N; i++) {
        cnt += table[i];
    }

    return cnt;
}

long solve(char *fn) {
    FILE *fp = fopen(fn, "r");
    long timer = 0;
    char c;

    if (fp == NULL)
        exit(EXIT_FAILURE);

    bzero(table, N);
    bzero(next_table, N);

    while(fscanf(fp, "%lu%c", &timer, &c) == 2) {
        table[timer]++;
        if(c == '\n') {
            break;
        }
    }
    fclose(fp);

    long i = 0;
    for(; i < 80; i++) {
        next_day();
    }
    printf("\nPart 1: after 80 days: %lu\n", count_fish());

    for(; i < 256; i++) {
        next_day();
    }
    printf("\nPart 2: after 80 days: %lu\n", count_fish());

    return 0;
}


int main() {
    /*print_grid();*/
    printf("part1: %lu\n", solve(FILENAME));
    /*printf("part2: %d\n", part2(FILENAME));*/


    return 0;
}
